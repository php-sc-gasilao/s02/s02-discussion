<?php

// Repetition Control Structures

// While loop: it takes a single condition. if the condition evaluates to true, the code inside the block will run.

function whileLoop(){
	$count = 5;

	while($count !==0){
		echo $count. '<br/>';
		$count--;
	}
}

// Do while loop: guarantees that the code will be executed at least once.

function doWhileLoop(){
	$count = 20;

	do{
		echo $count . '<br/>';
		$count--;
	}while ($count > 0);
}

// For Loop: more flexible that while and do-while loops

function forLoop(){
	for ($count = 0; $count <= 20; $count++){
		echo $count . '<br/>';
	}
}

// Array Manipulation

// PHP Arrays are declared using square brackets '[]' or array() function.

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1927'); //before PHP 5.4

$newStudentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1927']; //introduce on PHP 5.4

// Simple Arrays

$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Gateway', 'Toshiba'];

$task = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Associative array

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 90.1, 'thirdGrading' => 94.3, 'fourthGrading' => 89.2];

// 2-dimensional array

$heroes = [ 
	['iron man', 'thor', 'hulk'],
	['wolverine', 'cyclops', 'jean grey'],
	['batman', 'superman', 'wonder woman']
];

// 2-dimensional associative array
$ironManPowers = [
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']
];

// Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

// Sorting arrays
sort($sortedBrands); //sort brands accordingly
rsort($reverseSortedBrands); // sort brands in reverse

// Other Array Functions

function searchBrand($brands, $brand){
	return(in_array($brand, $brands)) ? "brand is in the array." : "$brand is not in the array.";
}