<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structures</h1>

		<h2>While Loop</h2>

		<?php whileLoop(); ?>

		<h2>Do-while Loop</h2>

		<?php doWhileLoop(); ?>

		<h2>For Loop</h2>

		<?php forLoop(); ?>

		<h1>Array Manipulation</h1>

		<h2>Types of Arrays</h2>

		<h3>Simple Array</h3>

		<ul>
			<?php foreach($computerBrands as $brand){ ?>
				<li><?php echo $brand ?></li>
			<?php } ?>

		</ul>

		<h3>Associative Array</h3>

		<ul>
			<?php foreach($gradePeriods as $period => $grade){ ?>
				<li>Grade in <?= $period ?> is <?= $grade ?></li>
			<?php } ?>
		</ul>

		<ul>
		<?php
			foreach($heroes as $team){
				foreach($team as $member){
				?>
				<li><?php echo $member ?></li>
				
				<?php }
			}
		?>

		</ul>

		<h2>Array Functions</h2>

		<h3>Sorting</h3>

		<pre><?php print_r($computerBrands) ?></pre>
		<pre><?php print_r($sortedBrands) ?></pre>

		<pre><?php print_r($reverseSortedBrands) ?></pre>

		<h3>Append</h3>

		<?php array_push($computerBrands, 'Apple') ?>

		<pre><?php print_r($computerBrands) ?></pre>

		<?php array_unshift($computerBrands, 'Dell') ?>
		
		<pre><?php print_r($computerBrands) ?></pre>

		<h3>Remove</h3>

		<?php array_pop($computerBrands) ?>
		
		<pre><?php print_r($computerBrands) ?></pre>

		<?php array_shift($computerBrands) ?>
		
		<pre><?php print_r($computerBrands) ?></pre>

		<h4>In Array</h4>

		<pre><?php echo searchBrand($computerBrands, 'HP' ) ?></pre>
		<h4>Count</h4>

		<pre><?php echo count($computerBrands) ?></pre>

		<!-- Link for Read: https://sabe.io/classes/php/arrays -->

	</body>
</html>